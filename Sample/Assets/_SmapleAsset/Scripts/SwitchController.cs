﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchController : MonoBehaviour {

    public GameObject caracterCam;
    public GameObject caracter;
    BasicBehaviour caracterScript00;
    MoveBehaviour caracterScript01;

    public GameObject flyCam;

    bool isControlCaracter = true;


    void Start () {
        caracterScript00 = caracter.GetComponent<BasicBehaviour>();
        caracterScript01 = caracter.GetComponent<MoveBehaviour>();

        flyCam.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if(isControlCaracter)
            {
                isControlCaracter = false;
                caracterCam.SetActive(false);
                caracterScript00.enabled = false;
                caracterScript01.enabled = false;
                flyCam.SetActive(true);
            }
            else
            {
                isControlCaracter = true;
                caracterCam.SetActive(true);
                caracterScript00.enabled = true;
                caracterScript01.enabled = true;
                flyCam.SetActive(false);
            }
        }
    }
}
